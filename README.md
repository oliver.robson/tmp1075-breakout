# TMP1075 Breakout

A KiCad project of a breakout for the Texas Instruments TMP1075DGK - https://www.ti.com/store/ti/en/p/product/?p=TMP1075DGKT
# Licence

All hardware schematics and design files are released under the CERN Open Hardware Licence v1.2. Please see [LICENSE](LICENSE) for details.
